#include <mysql.h>
#include <stdio.h>
#include <stdlib.h>
/*
 *  compiled using:
 *
 *  gcc testquery.c -o testquery `mysql_config --cflags --libs`
 *
 *  Clean up to use with interactive sql for faster commands, created as is for now to pipe results or grep quickly
 *
 */

char* q = "SELECT * FROM report.loan_trial_balance;";
char* pass = "34DolphinsSing"; //provide
char* user = "andy_rdbms_prod_mz"; //provide

void finish_with_error(MYSQL *con) {
  fprintf(stderr, "%s\n", mysql_error(con));
  mysql_close(con);
  exit(1);
}

int main(int argc, char **argv) {
 
  MYSQL *con = mysql_init(NULL);

  if (con == NULL) {
    fprintf(stderr, "mysql_init() failed\n");
    exit(1);
  }
  //implement when SSH Security is in place
  //unsigned int i = 1;  
  //mysql_options(con, MYSQL_OPT_SSL_MODE,&i);
  if (mysql_real_connect(con, "35.226.253.175", user, pass, "",3306,NULL,0) == NULL) {

    finish_with_error(con);
  }else{
    printf("%s\n","Connection established.");
  }

  if (mysql_query(con, q)) {
    finish_with_error(con);
  }

  MYSQL_RES *result = mysql_store_result(con);

  if (result == NULL) {
    finish_with_error(con);
  }
  int num_fields = mysql_num_fields(result);

  MYSQL_ROW row;

  while ((row = mysql_fetch_row(result))) {
    for (int i = 0; i < num_fields; i++) {
      printf("%s ", row[i] ? row[i] : "NULL");
    }
    printf("\n");
  }

  mysql_free_result(result);
  mysql_close(con);

  exit(0);
}
